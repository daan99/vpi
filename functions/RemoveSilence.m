function nosilence = RemoveSilence(fullaudio,threshold)
    % REMOVESILENCE  cuts period of silence at start and beginning of audio
    % fragment
    %   Inputs:
    %   - fullaudio: audio data containing the periods of silence
    %   - threshold: factor of maximum of noise
    %
    %   Outputs:
    %   - nosilence: audiodata without silence period
    %
    %   Authors: Daan Roos
    %   Date: 08-05-2020

    % Finds difference between samples in audio data
    df = diff(abs(fullaudio));
    
    % Calculates mean of this difference, when noise is present, mean will
    % lie just above/on largest noise value. Choosing the threshold will
    % find the position right where the audio starts
    valid_samples = find(df >= threshold*mean(abs(df)));
    
    % Full audio is cut into only non-silence
    nosilence = fullaudio(valid_samples(1):valid_samples(end));

end