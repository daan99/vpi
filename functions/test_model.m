function [output,data] = test_model(model,in,out)
    % TEST_MODEL  Finds model data such as accuracy, specificity, and
    % sensitivity. Also shows the confusion matrix in ascii, and figure
    % form.
    %   Inputs:
    %   - model: the structure containing the model.
    %   - in: the test-set data
    %   - out: the answers for the test-set data
    %   Outputs:
    %   - output: the output is a vector containing: accuracy, sensitivity,
    %   and specificity respectively
    %
    %   Authors: Daan Roos, Tycho van Velden
    %   Date: 7-5-2020
    figure;
    %% Testing model using test-set
    y_predicted = model.predictFcn(in);
    disp("Predicted: ");
    disp(y_predicted);

    %% Comparing predicted output to actual output
    test = classperf(out+1,y_predicted+1);

    %% Creating confusion matrix
    mat = confusionmat(out+1,y_predicted+1);
    confusionchart(out,y_predicted);
    
    %% Showing output of accuracy, sensitivity, and specificity
    acc = sum(diag(mat))/sum(sum(mat))
    
    disp("Accuracy:    " + acc + newline + "Sensitivity: " + test.Sensitivity + newline + "Specificity: " + test.Specificity + newline + "Matrix: ");
    disp(mat);
    data = calc_ssa(mat);
    
    
    %% Creating confusionmatrix using different method
    classes = unique(y_predicted);
    
    GT = zeros(length(classes),length(out));
    
    for index=1:length(classes)
        GT(index,out==classes(index)) = 1; 
    end

    Pred = zeros(length(classes),length(y_predicted));
    for index=1:length(classes)
        Pred(index,y_predicted==classes(index)) = 1; 
    end

    figure;
    plotconfusion(GT,Pred);
    
    %% Returning data
    output = [acc,test.Sensitivity,test.Specificity];
end