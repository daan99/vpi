function [varargout] = get_normdata(data,varargin)
    % GET_NORMDATA  Function that will normalize each collumn of features
    % for machine learning. The function allows variations in inputs and
    % outputs. When 1 output is requested, only the normalized data will be
    % given, when 2 outputs are requested, only the mean and std values
    % will be given. At 3 outputs, the function will return the data, mean,
    % and std in that order.
    % NORMALIZED_DATA = GET_NORMDATA(DATA)
    % NORMALIZED_DATA = GET_NORMDATA(DATA,MEAN,STD)
    % [NORMALIZED_DATA,MEAN,STD] = GET_NORMDATA(DATA)
    % [NORMALIZED_DATA,MEAN,STD] = GET_NORMDATA(DATA,MEAN,STD)
    %
    %   Inputs:
    %   - data: the matrix of features that should be normalized. Each
    %   column should contain a feature, with the rows represented the data
    %   for that feature. When the matrix is flipped, the property
    %   FlippedInput equal to 1 can be given.
    %   - [data] mean std: If the data should be normalized using existing
    %   mean and std, vectors containing these data can be given
    %   - [data] ___ Property, Value: Properties and their values can be
    %   given. Properties and possible values: 'FlippedInput', : when each
    %   row represents data instead of the collumns. ÍgnoreLast',: when the
    %   last collumn/row should be ignored.
    %
    %   Outputs:
    %   - normalized_data: the matrix containing the normalized input data
    %   - mean, std: the mean and std vectors of the input data
    %   - normalized_data, mean, std: the matrix containing the normalized
    %   data, and the vectors containing mean and std data of the input
    %   data
    %
    %   Authors: Daan Roos
    %   Date: 13-05-2020
    
    % Variable preperation
    meantouse = [];
    stdtouse = [];
    lastcol = [];
    remnan = 0;
    
    % Properties
    if(nargout > 3)
        error("Too much output arguments!");
    elseif(nargout == 0)
        return
    end
    
    if(~isempty(varargin))
        for arg = 1:length(varargin)
            switch char(varargin{arg})
                % When input is collumnvector in stead of row
                case 'FlippedInput'
                    if(varargin{arg+1} == 1)
                        data = data';
                    end
                    arg = arg + 1;
                % When last collumn contains the class value, this means
                % that the last value will not be normalized
                case 'IgnoreLast'
                    lastcol = data(:,end);
                    data = data(:,1:end-1);
                % Option that, when 1 is given, will remove NaN values
                case 'RemNaN'
                    remnan = varargin{arg+1};
                    arg = arg + 1;
                otherwise
                    % If more arguments are given that are doubles, these
                    % are the mean and std of previous data that should be
                    % used to normalize this data
                    argument = varargin{arg};
                    if(arg == 1 && isa(argument,'double'))
                        meantouse = argument;
                    elseif(arg == 2 && isa(argument,'double'))
                        stdtouse = argument;
                    end
            end
        end
    end
    
     % Preperation of variables after property changes
     if(isempty(meantouse)) meantouse = NaN(1,size(data,2)); end
     if(isempty(stdtouse)) stdtouse = NaN(1,size(data,2)); end
    
    for col = 1:size(data,2)
        collumn = data(:,col);
        % Calculation of mean and std
        if(isnan(meantouse(col))) meantouse(col) = nanmean(collumn); end
        if(isnan(stdtouse(col))) stdtouse(col) = nanstd(collumn); end
            
        % If the normalized data is also requested, input gets normalized
        if(nargout == 1 || nargout == 3)
            data(:,col) = NormalizeAudio(collumn,meantouse(col),stdtouse(col));
            if(isnan(data(:,col)))
                data(:,col) = 0;
            end
        end
    end
    
    % When the RemNaN option is given, all NaN values will be removed
    if((nargout == 1 || nargout == 3) && remnan == 1)
        data(:,isnan(data(1,:))) = [];
    end
    
    % Depending on the amount of outputs requested, certain data is
    % returned, see description
    switch nargout
        case 1
            if(~isempty(lastcol)) data(:,end+1) = lastcol; end
            varargout{1} = data;
        case 2
            varargout{1} = meantouse;
            varargout{2} = stdtouse;
        case 3
            if(~isempty(lastcol)) data(:,end+1) = lastcol; end
            varargout{1} = data;
            varargout{2} = meantouse;
            varargout{3} = stdtouse;
        otherwise
    end
end