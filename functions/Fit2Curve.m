function output = Fit2Curve(X, Y, order)
    % FIT2CURVE  This function finds the nth order polynomial fit of the
    % data Y(X) and returns the coefficients of the polynomial.
    %   Inputs:
    %   - X: The x data of the function
    %   - Y: The y data of the function at values of x
    %   - order: The order of the polynomial the function will approximate,
    %   the maximum order is 9
    %
    %   Outputs:
    %   - order: Vector containing the polynomial coefficients
    %
    %   Authors: Tycho van Velden
    %   Date: 13-05-2020
    
    X = X(:);
    % Fitting Y to a nth order polynomial
    curve = fit(X, Y, "poly" + string(order));

    % Storing coefficients in output vector
    output = zeros(order,1);
    for order_index = 1:order
        output(order_index) = curve.("p"+order_index); 
    end
end