classdef voiceActivation < handle
    % VOICEACTIVATION  This class containes functions and tools to record
    % audio 'without'delay by setting up the audio in the background. Also
    % gives a possibility to activate functions when someone is speaking or
    % return the audio of the window where the person was speaking.
    %
    %   Authors: Daan Roos
    %   Date: 13-05-2020
    %% Properties
    properties(Access = private)
        recordingStarted   % Time in seconds when the recorder started
        recordingEnded     % Time in seconds when the recorder ended
    end
    properties(SetAccess = protected, GetAccess = public)
        recorderObj        % Object of the recorder
        windowLength       % Length of a window in samples
        previousTime = seconds(duration([0,0,0])); % Last detection time
        Threshold          % Threshold of enviromental noise
    end
    properties(Access = public)
        SampleRate = 8000;
        Duration = 2;       % Seconds
        Channels = 1;
        Resolution = 16;    % Bits
        ThresholdTimes = 0.05;
        CallBack = [];
    end
    %% Methods
    methods
        % Starting recording
        function start(obj,SampleRate,Duration,Channels,Resolution)
                % START  This function starts a recorder object, when no
                % arguments are given, the default settings of a samplerate
                % of 8000, a duration of 3 seconds, 1 channel and a
                % resolution of 16 bits is used. This function will start
                % by measuring the enviromental noise, which takes a few
                % seconds.
                %   Inputs:
                %   - obj: The voiceActivation class created
                %   - SampleRate: Optional, the samplerate used by the
                %   recorder
                %   - Duration: Optional, the duration of a recording
                %   - Channels: Optional, the amount of channels used in a
                %   recording
                %   - Resolution: Optional, the resolution of the recording
                %
                %   Authors: Daan Roos
                %   Date: 13-05-2020
                
            % When inputs are given, the associated setting will be changed
            if(nargin > 1)
                if(nargin > 2)
                    obj.SampleRate = SampleRate;
                    obj.Duration = Duration;
                elseif(nargin > 3)
                    obj.SampleRate = SampleRate;
                    obj.Duration = Duration;
                    obj.Channels = Channels;
                elseif(nargin > 4)
                    obj.SampleRate = SampleRate;
                    obj.Duration = Duration;
                    obj.Channels = Channels;
                    obj.Resolution = Resolution;
                else
                    obj.SampleRate = SampleRate;
                end 
            end
            
            % An audiorecorder object will be created
            obj.recorderObj = audiorecorder(obj.SampleRate,obj.Resolution,obj.Channels);
            
            % The length of a window in samples will be set, equal to 1.5
            % times the duration of a recording
            % The windowlength indicates for how long the recorder looks
            % for audio to appear
            obj.windowLength = ceil(1.5*obj.Duration*obj.SampleRate);
            
            % The previous time audio was detected will be reset
            obj.previousTime = seconds(duration([0,0,0]));
            
            % Time in seconds when recording is started
            obj.recordingStarted = datetime();
            
            % Recorder object is activated
            record(obj.recorderObj);
            
            % Start of measuring enviromental noise
            pause(1.5);
            
            % Get a window of the audio
            temp_window = get_window(obj);
            
            % Detect the threshold, 2 times the maximum amplitude
            obj.Threshold = 2*max(abs(temp_window));
            
            disp("Recording started!");
        end
        
        % Stopping recording
        function stop(obj)
                % STOP  Stops the audiorecording object
                %
                %   Authors: Daan Roos
                %   Date: 13-05-2020
            if(isempty(obj.recorderObj))
                disp("Nothing to stop!");
                return;
            end
            
            stop(obj.recorderObj);
            
            obj.recorderObj = [];
            
            % Saves time when recorder has been stopped
            obj.recordingEnded = datetime();
            
            disp("Recording stopped!");
        end
        
        % Retreive window of audio
        function window = get_window(obj)
                % GET_WINDOW  Depending on the length of a window, will
                % return the last set of recorder audio data with the
                % length of a window.
                %   Inputs:
                %   - obj: this voiceActivation class with a recorder
                %   running
                %
                %   Outputs:
                %   - window: the audio data associated to the window
                %
                %   Authors: Daan Roos
                %   Date: 13-05-2020
            data = getaudiodata(obj.recorderObj);
            
            if(length(data)-1 < obj.windowLength)
                window = data;
            else
                window = data(end-obj.windowLength-1:end,:);
            end
        end
        
        % Record audio
        function recording = record(obj,time)
                % RECORD  Will wait for time seconds and gets the data
                % recorder in this time. Usefull to be able to have
                % realtime recordings, where calling the function will
                % instantly start a recording.
                %   Inputs:
                %   - obj: this voiceActivation class created with a
                %   recorder running
                %   - time: duration in seconds of a recording
                %
                %   Outputs:
                %   - recording: the audio data associated to the recorded
                %   time
                %
                %   Authors: Daan Roos
                %   Date: 13-05-2020
            pause(time);
            data = getaudiodata(obj.recorderObj);
            
            recording = data(end-(obj.SampleRate*time)-1:end,:);
        end
        
        % Test for command in voice
        function window = check_voice(obj)
                % CHECK_VOICE  Function that can be called to check for
                % audio in the last window. Will return the window
                % containing the audio when audio was present. Can also
                % activate the callback set in this class when audio has
                % been detected.
                %   Inputs:
                %   - obj: this class with a running recorder
                %
                %   Outputs:
                %   - window: the window of recorded audio data containing
                %   the audio that has been detected. When no audio is
                %   detected, a 0 will be returned.
                %
                %   Authors: Daan Roos
                %   Date: 13-05-2020
            window = 0;
            if(isempty(obj.recordingStarted))
                disp("Recording has not started yet!");
                return;
            end
            
            % Calculates time passed since start of recording, will used
            % to determine whether this time is longer than the previous
            % time audio was detected
            now = seconds(datetime()-obj.recordingStarted);
            if(obj.previousTime < now)
                % Gets last window
                window = get_window(obj);
                
                % If somewhere in the window, audio passes the threshold
                % detected on startup, and this audio is within the last
                % and first 10 percent of the data, audio has been detected
                nosilence = find(window > obj.Threshold);
                if(length(nosilence) > 1 && nosilence(end) < 0.9*obj.windowLength && nosilence(1) > 0.1*obj.windowLength)
                    disp("Voice present in data!");
                    % New time, when the last audio was detected,
                    % will be set
                    obj.previousTime = now + nosilence(end)/obj.SampleRate;
                    
                    % If a callback has been set, callback will be executed
                    if(~isempty(obj.CallBack))
                        obj.CallBack(window);
                    end
                else
                    window = 0;
                end
            end
        end
    end
end