function [S,F,T,ps] = spectfunction(audio,Fs,duration,overlap,toplot,axtouse)
    % SPECTFUNCTION  creates the spectogram of the given audio
    %   Inputs:
    %   - audio: recorded audio data with stripped off silence periods
    %   - Fs: sampling frequency of the recorded data
    %   - duration: duration of one window
    %   - overlap: percentage overlap per window
    %   - toplot: optional argument, when given a spectogram will be
    %   plotted
    %   - axtouse: axis object to plot to instead of clean plot
    %
    %   Outputs:
    %   - s: spectogram data
    %   - f: frequency axis data
    %   - t: time axis data
    %
    %   Authors: Daan Roos, Tycho van Velden
    %   Date: 08-05-2020
    N = duration * Fs;
    if(N > length(audio)) error("Input audio too short!" + newline + "Make sure that the length of the audio is equal to the width of a window times the sample rate!"); end
    hann_window = hann(N);
    [S,F,T,ps] = spectrogram(audio,hann_window,overlap*N,[],Fs);
    if(nargin>=5)
        if(toplot == 0)
            return
        end
        if(nargin == 5)
            spectrogram(audio,hann_window,overlap*N,F,Fs,'yaxis'); 
        else
            if(T(length(T)) <= 1)
                Tscaled = T*1000;
            else
                Tscaled = T;
            end
            imagesc(axtouse,Tscaled, F/1000, 10*log10(abs(ps)));
            set(axtouse,'YDir', 'normal');
            br = colorbar(axtouse);
            ylabel(br, 'Power/Frequency (dB/Hz)');
            if(T(length(T)) <= 1)
                xlabel(axtouse,"Time (ms)");
            else
                xlabel(axtouse,"Time (s)");
            end
            ylabel(axtouse,"Frequency (kHz)");
            axis(axtouse,"xy")
            axis(axtouse, "tight")
        end
    end
end