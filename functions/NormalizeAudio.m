function audioout = NormalizeAudio(audio,mn,stdaud)
    % NORMALIZEAUDIO  Normalizes the audio by either finding the mean and
    % std of the audio itself, or by using the mean and std given as input.
    %   Inputs:
    %   - audio: the audio data that will be normalized
    %   - mn: the mean which will be used to normalize the audio data
    %   - stdaud: std wich will be used to normalize the audio data
    %
    %   Outputs:
    %   - audioout: the normalized audio data
    %
    %   Authors: Daan Roos, Tycho van Velden
    %   Date: 12-05-2020
    
    % Find mean and std when no mean and std where given as arguments,
    % thus for training-data
    if(nargin == 1)
        mn = mean(audio);
        stdaud = std(audio);
    end
    
    audioout = (audio-mn)/stdaud;
end