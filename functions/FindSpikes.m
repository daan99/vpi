function [pospeaks,peakswidth,peaksheight,direction,diffpeaks] = FindSpikes(audio,window,datapoints)
    % FINDSPIKES  Uses envelope detection to find peaks in amplitude.
    % Creates data according to these peaks such as the width of a peak,
    % the height, how much the height changes compared to the previous
    % peaks.
    %   Inputs:
    %   - audio: Input audio with silence removed
    %   - window: The window (in samples) used to create the envelope
    %   - datapoint: the maximum amount of peaks that should be tested
    %
    %   Outputs:
    %   - pospeaks: the relative position of the peaks
    %   - peakswidth: the relative width of the peaks
    %   - peaksheight: the relative height of the peaks
    %   - direction: whether the peaks goes down or up compared to the
    %   previous peak
    %   - diffpeaks: the relative difference in height between peaks
    %
    %   Authors: Daan Roos
    %   Date: 14-05-2020

    env = envelope(abs(audio),window,'peak');
    
    [totalpeaks, totalpospeaks] = findpeaks(env);
    [totaldips, totalposdips] = findpeaks(max(env)-env);
    
    if(length(totalpeaks) >= datapoints)
        peaks = totalpeaks(1:datapoints);
        pospeaks = totalpospeaks(1:datapoints);
    else
        peaks = [totalpeaks;zeros(datapoints-length(totalpeaks),1)];
        pospeaks = [totalpospeaks;zeros(datapoints-length(totalpeaks),1)];
    end
    if(length(totaldips) >= datapoints)
        dips = totaldips(1:datapoints);
        posdips = totalposdips(1:datapoints);
    else
        dips = [totaldips;zeros(datapoints-length(totaldips),1)];
        posdips = [totalposdips;zeros(datapoints-length(totaldips),1)];
    end
    
    pospeaks = pospeaks./length(audio);
    peakswidth = (pospeaks-posdips)./length(audio);
    peaksheight = (peaks-dips)./max(peaks);
    direction = sign(diff(peaks));
    diffpeaks = diff(peaks)./max(peaks);
    peaksheight(find((abs(peaksheight) == Inf))) = 0;
    pospeaks(find((abs(pospeaks) == Inf))) = 0;
    peakswidth(find((abs(peakswidth) == Inf))) = 0;
    diffpeaks(find((abs(diffpeaks) == Inf))) = 0;
    peaksheight(find(isnan(peaksheight))) = 0;
    pospeaks(find(isnan(pospeaks))) = 0;
    peakswidth(find(isnan(peakswidth))) = 0;
    diffpeaks(find(isnan(diffpeaks))) = 0;
end