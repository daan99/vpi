function class = GuiToClass(audiorecorder,struct_in,duration)
    % GUITOCLASS  Starts an audiorecorder, records duration amount of
    % seconds, gets audiofeatures, and predicts the class.
    %   Inputs:
    %   - audiorecorder: audiorecorder of type voiceActivation
    %   - struct_in: structure containing model, mean and std
    %   - duration: duration of audio recording
    %
    %   Outputs:
    %   - class: number of class detected
    %
    %   Authors: Daan Roos
    %   Date: 15-05-2020
    
    % Uses audiorecorder class to record duration seconds of audio
    audio = record(audiorecorder,duration);

    % Extracts features using evaluateall
    [audiofeatures,var] = evaluateall(audio,8000,0.02,0.5,3,1,0);
    % Normalizes features using mean and std stored in model structure
    normalizedfeatures = get_normdata(audiofeatures,struct_in.Mean,struct_in.Std,'IgnoreLast');
    % Predicts class using model stored in structure
    [class, certainty] = struct_in.Model.predictFcn(normalizedfeatures(:,1:end-1));
    
end