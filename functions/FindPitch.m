function [f0,loc] = FindPitch(audio,Fs,duration,overlap,toplot,axtouse)
    % FINDPITCH  finds data that follows the ground frequency of the audio
    %   Inputs:
    %   - audio: recorded audio with stripped silence
    %   - Fs: sampling frequency of the audio
    %   - duration: the duration of a window
    %   - overlap: the overlap percentage of each window
    %   - toplot: optional variable, when given, the plot of the line will
    %   be created
    %   - axtouse: axis object to plot to instead of clean plot
    %
    %   Outputs:
    %   - f0: values of the ground frequency at each time index
    %   - loc: samples associated to the ground frequencies given in f0
    %
    %   Authors: Daan Roos, Tycho van Velden
    %   Date: 08-05-2020
    N = round(duration*Fs);
    [f0,loc] = pitch(audio, Fs, 'Method', 'CEP', 'WindowLength', N, 'OverlapLength', round(overlap*N));
    if(nargin>=5)
        if(toplot == 0)
            return
        end
        if(length(audio)/Fs < 1)
            time = loc/Fs * 1000;
        else
            time = loc/Fs;
        end
        if(nargin == 5)
            hold on;
            plot(time,f0/1000, 'Color', 'r','LineWidth',2);
            text(time(round(0.1*length(time))),f0(round(0.1*length(time)))/1000+0.2,'F_0','FontSize',18,'Color','r');
            hold off;
        else
            hold(axtouse,'on');
            plot(axtouse,time,f0/1000, 'Color', 'r','LineWidth',2);
            text(axtouse,time(round(0.1*length(time))),f0(round(0.1*length(time)))/1000+0.2,'F_0','FontSize',18,'Color','r');
            hold(axtouse,'off');
        end
    end
end