function output = calc_ssa(matrix)
    % CALC_SSA  Finds the sensitivity, specificity, and accuracy for each
    % class in a matrix. Returns a table containing this data and the
    % factor of measure.
    %   Inputs:
    %   - matrix: the input confusionmatrix
    %
    %   Outputs:
    %   - output: matrix containing accuracy, sensitivity, specificity, and
    %   factor of measure for each class
    %
    %   Authors: Daan Roos, Tycho van Velden
    %   Date: 11-05-2020
    output = zeros(size(matrix,1),4);
    for class=1:size(matrix,1)
        tp = matrix(class,class);
        rtp = sum(matrix(class,:));
        sen = tp/rtp;
        tn = matrix;
        tn(:,class) = [];
        fp = sum(sum(tn));
        tn(class,:) = [];
        tn = sum(sum(tn));
        spec = tn/fp;
        acc = (tp+tn)/sum(sum(matrix));
        factor = acc*sen*spec;
        output(class,:) = [acc,sen,spec,factor];
    end
    Accuracy = output(:,1);
    Sensitivity = output(:,2);
    Specificity = output(:,3);
    FactorOfMeasure = output(:,4);
    table(Accuracy,Sensitivity,Specificity,FactorOfMeasure)
end