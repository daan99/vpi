function [t_vec,frmtns] = FormantsFinder(audio,Fs,duration,overlap,toplot,axtouse)
    % FORMANTSFINDER  Uses the FormantsEpo4 function to find the formants
    % in the audio signal. Also supports plotting the output data into
    % either a new plot of a existing axes object.
    %   Inputs:
    %   - audio: recorded audio data with silence periods removed.
    %   - Fs: the sample rate of the recording.
    %   - duration: the window used to find the formants in seconds.
    %   - overlap: the percentage of overlap of each window.
    %   - toplot: when this argument is set, the function will plot the
    %   data obtained.
    %   - axtouse: when the plot should be created in an already existing
    %   axes object, this argument can be defined.
    %
    %   Outputs:
    %   - t_vec: the vector containing the time related to the formant
    %   frequencies found.
    %   - frmtns: a matrix containing the frequencies of the formants. The
    %   columns contain row vectors with the frequencies per formant.
    %
    %   Authors: Daan Roos, Tycho van Velden
    %   Date: 11-05-2020
    % Converting seconds to samples, running the FormantsEpo4 function
    L = round(duration*Fs);
    [t_vec,frmtns,f0] = FormantsEpo4(audio,Fs,L,round(L*overlap));
    % When toplot is defined, a plot should be created
    if(nargin>=5)
        if(toplot == 0)
            return
        end
        % When the audio is shorter than 1 second, the plot will be created
        % using milliseconds on the x-axes
        if(length(audio)/Fs < 1)
            time = t_vec * 1000;
        else
            time = t_vec;
        end
        % When axtouse is not defined, the plot is created on the latest
        % used plot, or a new one will be created.
        if(nargin == 5)
            hold on;
            plot(time,frmtns(:,1)/1000, 'Color', 'k');
            plot(time,frmtns(:,2)/1000, 'Color', 'k');
            plot(time,frmtns(:,3)/1000, 'Color', 'k');
            % A label will be added next to the formant indicating whether
            % it is the 1st, 2nd, or 3rd formant
            text(time(round(0.1*length(time))),frmtns(round(0.1*length(time)),1)/1000+0.2,'F_1','FontSize',18,'Color','k');
            text(time(round(0.1*length(time))),frmtns(round(0.1*length(time)),2)/1000+0.2,'F_2','FontSize',18,'Color','k');
            text(time(round(0.1*length(time))),frmtns(round(0.1*length(time)),3)/1000+0.2,'F_3','FontSize',18,'Color','k');
            hold off;
        % When axtouse is defined, the plot will be created on the axtouse
        % object.
        else
            hold(axtouse,'on');
            plot(axtouse,time,frmtns(:,1)/1000, 'Color', 'k');
            plot(axtouse,time,frmtns(:,2)/1000, 'Color', 'k');
            plot(axtouse,time,frmtns(:,3)/1000, 'Color', 'k');
            % A label will be added next to the formant indicating whether
            % it is the 1st, 2nd, or 3rd formant
            text(axtouse,time(round(0.1*length(time))),frmtns(round(0.1*length(time)),1)/1000+0.2,'F_1','FontSize',18,'Color','k');
            text(axtouse,time(round(0.1*length(time))),frmtns(round(0.1*length(time)),2)/1000+0.2,'F_2','FontSize',18,'Color','k');
            text(axtouse,time(round(0.1*length(time))),frmtns(round(0.1*length(time)),3)/1000+0.2,'F_3','FontSize',18,'Color','k');
            hold(axtouse,'off');
        end
    end
end