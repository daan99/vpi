    % VOICEACTIVATIONSCRIPT  uses voiceActivation class to activate the
    % machine learning model when audio is detected.
    %
    %   Authors: Daan Roos
    %   Date: 13-05-2020

recordclass = voiceActivation;
load modelstruct.mat;
model = task2modelg;
trainingmean = model.Mean;
trainingstd = model.Std;
%%
start(recordclass);
%%
correct = 0;
classguessed = zeros(1,30);

direction = ["Initialize","Go","Brake"];
disp("Say: " + direction(1));
det = 1;
running = true;
while running
    window = check_voice(recordclass);

    if(length(window) ~= 1)
        [matm1,var] = evaluateall(window,8000,0.02,0.5,3,1,0);
        [normmatm1, testmean, teststd] = get_normdata(matm1,trainingmean,trainingstd,'IgnoreLast');
        [class certainty] = model.Model.predictFcn(normmatm1(:,1:end-1));
        disp(direction(class+1));
        disp(class);
        disp(certainty);
        if(certainty(class+1) < 0.99) disp("Uncertain!"); end
        if((floor(det/10)+1) == class+1) correct = correct + 1; end
        classguessed(det) = class;
        det = det + 1;
        if(det == 31) running = false;
        else disp("Say: " + direction(floor(det/10)+1));
        end
    end
end

disp("Correctly guessed: " + string(correct) + "/30");
disp("Guesses:" + join(string(classguessed),'-'));
%%
stop(recordclass);
