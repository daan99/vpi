function dataout = data_extract(audio,f0,formants,t_formants)
    % DATA_EXTRACT  extracts properties used for training and testing the
    % machine learning model. Properties contains: variance, mean, median
    % and more. The data used to find these properties is the fundamental
    % frequency and the formants.
    %   Inputs:
    %   - f0: Vector containing data for the fundamental frequency obtained
    %   using pitch
    %   - formants: matrix in format: [f1, f2, f3]. Contains a vector of
    %   frequencies for each formant
    %   - t_formants: time vector associated with the formants matrix
    %
    %   Outputs:
    %   - dataout: A structure containing all the data extracted as
    %   mentioned in the description. Also contains the input data for
    %   reference.
    %
    %   Authors: Daan Roos
    %   Date: 11-05-2020
    
    % All NaN entries will be removed from the data
    f1 = formants(:,1);
    f2 = formants(:,2);
    f3 = formants(:,3);
    ratiof1f2 = f1./f2;
    ratiof1f3 = f1./f3;
    ratiof2f3 = f2./f3;
    t1 = t_formants;
    t2 = t_formants;
    t3 = t_formants;
    t1(find(isnan(f1))) = [];
    t2(find(isnan(f2))) = [];
    t3(find(isnan(f3))) = [];
    f1(find(isnan(f1))) = [];
    f2(find(isnan(f2))) = [];
    f3(find(isnan(f3))) = [];
    ratiof1f2(find(isnan(ratiof1f2))) = [];
    ratiof1f3(find(isnan(ratiof1f3))) = [];
    ratiof2f3(find(isnan(ratiof2f3))) = [];
    
    % Finding variance
    varf0 = var(f0); 
    varf1 = var(f1);
    varf2 = var(f2); 
    varf3 = var(f3);
    varratio = var(ratiof1f2);
    varratiof1f2 = var(ratiof1f2);
    varratiof1f3 = var(ratiof1f3);
    varratiof2f3 = var(ratiof2f3);

    % Finding mean
    meanf0 = mean(f0);
    meanf1 = mean(f1);
    meanf2 = mean(f2);
    meanf3 = mean(f3);
    meanratio = mean(ratiof1f2);
    meanratiof1f2 = mean(ratiof1f2);
    meanratiof1f3 = mean(ratiof1f3);
    meanratiof2f3 = mean(ratiof2f3);
    
    % Finding median
    medianf0 = median(f0);
    medianf1 = median(f1);
    medianf2 = median(f2);
    medianf3 = median(f3);
    medianratio = median(ratiof1f2);
    medianratiof1f2 = median(ratiof1f2);
    medianratiof1f3 = median(ratiof1f3);
    medianratiof2f3 = median(ratiof2f3);
    
    % Audio features
    len = length(audio);
    E = sum(abs(audio).^2);
    [pospeaks,peakswidth,peaksheight,direction,diffpeaks] = FindSpikes(audio,300,3);
    
    % Polynomial fitting
    p11 = Fit2Curve(t1, f1, 1);
    p21 = Fit2Curve(t2, f2, 1);
    p31 = Fit2Curve(t3, f3, 1);
    
    % The change of the formants
    change1 = FormantChange(f1, 4, 5);
    change2 = FormantChange(f2, 4, 5);
    change3 = FormantChange(f3, 4, 5);
 
    % All data will be stored in a structure which makes reading the data
    % and requesting data easier
    dataout = struct( ...
        'Data',struct('f0',f0,'f1',f1,'f2',f2,'f3',f3,'ratiof1f2',ratiof1f2), ...
        'Variance',struct('f0',varf0,'f1',varf1,'f2',varf2,'f3',varf3,'ratiof1f2',varratiof1f2,'ratiof1f3',varratiof1f3,'ratiof2f3',varratiof2f3), ...
        'Mean',struct('f0',meanf0,'f1',meanf1,'f2',meanf2,'f3',meanf3,'ratiof1f2',meanratiof1f2,'ratiof1f3',meanratiof1f3,'ratiof2f3',meanratiof2f3), ...
        'Median',struct('f0',medianf0,'f1',medianf1,'f2',medianf2,'f3',medianf3,'ratiof1f2',medianratiof1f2,'ratiof1f3',medianratiof1f3,'ratiof2f3',medianratiof2f3), ...
        'AudioFeatures',struct('Length',len,'pospeaks',pospeaks,'peakswidth',peakswidth,'peaksheight',peaksheight,'direction',direction,'diffpeaks',diffpeaks), ...       
        'Change',struct('Change1',change1,'Change2',change2,'Change3',change3), ...
        'Polynomial1',struct('p1',p11), ...
        'Polynomial2',struct('p1',p21), ...
        'Polynomial3',struct('p1',p31) ...
    );

%     dataout = struct( ...
%         'Data',struct('f0',f0,'f1',f1,'f2',f2,'f3',f3,'ratiof1f2',ratiof1f2), ...
%         'Variance',struct('f0',varf0,'f1',varf1,'f2',varf2,'f3',varf3,'ratiof1f2',varratio), ...
%         'Mean',struct('f0',meanf0,'f1',meanf1,'f2',meanf2,'f3',meanf3,'ratiof1f2',meanratio), ...
%         'Median',struct('f0',medianf0,'f1',medianf1,'f2',medianf2,'f3',medianf3,'ratiof1f2',medianratio), ...
%         'AudioFeatures',struct('Length',len,'pospeaks',pospeaks,'peakswidth',peakswidth,'peaksheight',peaksheight,'direction',direction,'diffpeaks',diffpeaks), ...       
%         'Change',struct('Change1',change1,'Change2',change2,'Change3',change3), ...
%         'Polynomial1',struct('p1',p11), ...
%         'Polynomial2',struct('p1',p21), ...
%         'Polynomial3',struct('p1',p31) ...
%     );
end