function [dataset,variables] = setcreate(datastruct,classid)
    % SETCREATE  Turns the data contained in a structure into a column
    % vector. Adds the class-id defined in the input classid. The vectors
    % can later be merged with other vectors to create either a training or
    % a test set.
    %   Inputs:
    %   - datastruct: Structure containing the data. Should be build as
    %   follows: totaldata, variable1,variable2,variable3 etc. The total
    %   data (which will be ignored) should be the first field in the
    %   structure.
    %   - classid: The class associated to the entered data.
    %
    %   Outputs:
    %   - dataset: Column vector containg all the data and ending with the
    %   class-id.
    %   - variables: cell array containing the names of the variables.
    %
    %   Authors: Daan Roos
    %   Date: 11-05-2020
    
    % Find all field in the structure
    types = fields(datastruct);
    % Find all subfields in the structure
    vars = fields(datastruct.(types{1}));
    
    % Prepare the output as a column vector with (field-1)*subfield + 1
    % length, and a cell array containing variable names
    %dataset = zeros(1,(length(types)-1)*length(vars) + 1);
    dataset = [];
    variables = {};
    % Last entry is the class-id
    %dataset(length(dataset)) = classid;
    datasetid = 1;
    % Extract data for each variable
    for fieldid = 2:length(types)
        field = types{fieldid};
        vars = fields(datastruct.(field));
        for varid = 1:length(vars)
            var = vars{varid};
            % When no data is defined at the location in the structure, the
            % entry will be NaN, which can be removed later when the total
            % dataset has been created
            variables{datasetid} = [field,'_',var];
            try
                % When the data is an array/a vector
                if(length(datastruct.(field).(var)) > 1)
                    % Multiple variable names will be created
                    for vec=1:length(datastruct.(field).(var))
                        variables(datasetid+vec-1) = {[field,'_',var,char(string(vec))]};
                    end
                    
                    % Depending on orientation, data will be stored in
                    % vector
                    if(size(datastruct.(field).(var),1) > 1)
                        dataset(datasetid:datasetid+length(datastruct.(field).(var))-1) = datastruct.(field).(var)';
                    else
                        dataset(datasetid:datasetid+length(datastruct.(field).(var))-1) = datastruct.(field).(var);
                    end
                    datasetid = datasetid + length(datastruct.(field).(var)) - 1;
                else
                    dataset(datasetid) = datastruct.(field).(var);
                end
            catch ME
                dataset(datasetid) = NaN;
                disp(ME);
            end
            datasetid = datasetid + 1;
        end
    end
    % Class id will be placed at the end of the vector
    variables{length(variables)+1} = 'Class';
    dataset(length(dataset)+1) = classid;
end