function output = FormantChange(formant, samples_per_frame, limit)
    % FORMANTCHANGE  Finds the change over time in each frame of the
    % formant entered in the arguments.
    %   Inputs:
    %   - formant: vector containing the data of the formant over time
    %   - samples_per_frame: the length of a frame
    %   - limit: the maximum amount of frames that should be calculated
    %
    %   Outputs:
    %   - output: vector with length of limit containing the change values
    %   of each frame
    %
    %   Authors: Tycho van Velden
    %   Date: 14-05-2020

    %Create Matrices
    framed_audio = zeros(samples_per_frame, ceil(length(formant)/samples_per_frame));
    means_frames = zeros(ceil(length(formant)/samples_per_frame),1);
    
    %Take the mean of every frame by first taking the range, and then
    %taking the mean of that range of values from the formant
    for i = 1:size(framed_audio,2)
        %define range
        range = (samples_per_frame *( i-1 ) + 1 ): (samples_per_frame * i);
        
        %make sure the final range does not exceed the length of the
        %formant
        if( samples_per_frame * i <= length(formant) )
            framed_audio(:,i) = formant(range);
        else
            last_frame = formant( (samples_per_frame *( i-1 ) + 1 ) : end);
            framed_audio(1:length(last_frame),i) = last_frame;
        end
        
        %create the mean of the frame
        means_frames(i) = mean(framed_audio(:,i));

    end
    
    %take the difference of all the mean to see if the formant is ascending
    %or descending between the two frames and with how much
    output = diff(means_frames);
    output = output(:);

    if(length(output) < limit)
        output = [output;zeros(limit-length(output),1)];
    else
        output = output(1:limit);
    end
end