function [datamat,variables,mean_std,mean_mean,varargout] = evaluateall(voices,Fs,window,overlap,threshold,orientation,classid,varargin)
    % EVALUATEALL  Runs the functions to extract properties from each audio
    % files contained in the voices matrix. Several options to tweak the
    % output: Table, can turn a table on or off, will appear as a variable
    % output, Range, a matrix containing the ids of the audio clips from
    % the input matrix that should be used. RemNaN, decides whether columns
    % only containing NaN should be removed.
    %   Inputs:
    %   - voices: the input matrix containing all the audio files
    %   - Fs: the sample rate of the recorded audio
    %   - window: the window that has to be used to determine the
    %   properties
    %   - overlap: the percentage overlap that should be used to determine
    %   the properties
    %   - threshold: the threshold at which audio will be declared
    %   non-silent. Used in the silent period removal
    %   - orientation: when set to 1, it is assumed that each column
    %   represents an audio clip, when 0 rows are represented as audioclips
    %   - classid: the number that indicates what class the audio files are
    %   - properties: Range, Table, RemNaN
    %
    %   Outputs:
    %   - datamat: the matrix containing every property for each audio
    %   clip. The columns represent the properties, rows the audio clips
    %   - variables: cell array containing the names of the properties
    %   associated with the matrix
    %   - variable output: could output the table when Table property is
    %   set to 1.
    %
    %   Authors: Daan Roos
    %   Date: 12-05-2020
    % When the rows contain the data in stead of column: flipped
    if(orientation == 1)
        matrix = voices;
    else
        matrix = voices';
    end
    
    tbl = 0;
    nans = 1;
    outputstruct = 0;
    
    % Properties of function. Changes behaviour of function depending on
    % properties added as variable input.
    if(~isempty(varargin))
        for arg = 1:length(varargin)
            switch char(varargin{arg})
                case 'Range'
                    matrix = matrix(:,varargin{arg+1});
                    arg = arg + 1;
                case 'Table'
                    tbl = varargin{arg+1};
                    arg = arg + 1;
                case 'RemNaN'
                    nans = not(varargin{arg+1});
                    arg = arg + 1;
                case 'OutputStruct'
                    outputstruct = 1;
                otherwise
            end
        end
    end
    
    datamat = [];
    % Extracting the data per audio clip
    for voice=1:size(matrix,2)
        % Get one clip of audio from input matrix
        audio = voices(:,voice);
        
        % Removing silence from audio
        nosilence = RemoveSilence(audio,threshold);
        
        % Normalizing audio
        normalized = NormalizeAudio(nosilence);

        %[s,f,t,ps] = spectfunction(nosilence,Fs,window,overlap);

        % Finding pitch (fundamental frequency) from audio
        [f0,loc] = FindPitch(normalized,Fs,window,overlap);

        % Finding formants from audio
        [t_vec,frmtns] = FormantsFinder(normalized,Fs,window,overlap);

        % Using created data in previous functions to create features
        dataout = data_extract(normalized,f0,frmtns,t_vec);
        
        % Saving created structure outside of function
        if(outputstruct == 1) assignin('base','ExtractedStructure',dataout); end
        
        % Transforming structure into vector
        [datamat(voice,:),variables] = setcreate(dataout,classid);
        
    end
    
    % When NaN removal is required, will search columns containing only NaN
    % and will remove them
    if(~nans)
        pos = 1;
        for col=1:size(datamat,2)
            if(prod(isnan(datamat(:,pos))))
                datamat(:,pos) = [];
                variables(pos) = [];
                pos = pos - 1;
            end
            pos = pos + 1;
        end
    end
    
    % When table output is required, will display a table with all the
    % properties, will also place the table on variable output
    if(tbl == 1)
        varargout{1} = array2table(datamat,'VariableNames',variables);
        disp(varargout{1});
    end
end