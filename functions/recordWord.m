function audio = recordWord(time,Fs)
    % RECORDWORD  records audio for a duration of time seconds
    %   Inputs:
    %   - time: time in seconds to record
    %   - Fs: sampling frequency of audio
    %
    %   Outputs:
    %   - audio: recorded audio output
    %
    %   Authors: Daan Roos, Tycho van Velden
    %   Date: 08-05-2020
    % TODO: Insert record sound
    recObj = audiorecorder(Fs,16,1); % create audio object, 16 bits resolution
    disp('Start speaking...');
    recordblocking(recObj, time); % do a 2 second recording (blocking)
    disp('End of Recording.');
    audio = getaudiodata(recObj);
end