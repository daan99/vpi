# VPI

Repository containing the code for the Voice Processing Interface.

Below, more information regarding the VPI is written. Using this information, the VPI can be used.

This program has been created by Daan Roos

## Files

### Installer

In root, the file: VoiceProcessingInterface.mlappinstall contains the installer for the matlab app VPI. This can be installed on MATLAB 2020a.

### Project

If the user wishes to see the code of the interface, the file vp.mlapp contains the full interface code.

### Functions

All the functions used for the Voice Processing module, which can be used in the Interface itself, can be found in the functions folder.

## Tutorial

When the interface has been installed using the installer, and the app is started, the user will be greated with a screen like the figure below.

![Image showing vpi interface](/images/interface.png)

To describe how the interface works, each tab will be explained seperatly.

### Settings tab

![Image showing settings tab](/images/settings.png)

This tab shows a list of settings that can be tweaked.

* **Record duration** The length of a new recording in seconds.
* **Sample Rate** The sample rate of a recording in Hz.
* **Silence Threshold** the threshold used to remove silences using your function.
* **Window** The window length in seconds.
* **Overlap** The overlap of windows, number from 0 to 1.
* **Input data** When the switch is set to 'Single', the data selected using the variable selector should be a row vector containing one audio recording. When set to 'Matrix', the input data is expected to be a matrix containing multiple audio recordings.

### Functions tab

![Image showing functions tab](/images/functions.png)

This tab supports the editing of functions and adding new functions.

* **Taskname** The name that will appear describing the function
* **Function** The name of the function in matlab
* **Arguments** Which arguments are needed. These should be seperated by ','. A few arguments are readily available: nosilence, the audio without silence, audioData, the data of the audio with silence periods, silencethreshold, the threshold indicating silence set in the settings tab, normalized, the normalized audio, Fs, the sample rate set in the settings tab, window, the window length set in the settings tab, overlap, the percentage overlap set in the settings, datamenu, the extracted features in structure form, classid, the class id set in the plot tab. More arguments will be created when they are mentioned. Numbers can also be added.
* **Outputs** Which outputs are needed, all arguments available can also be used as output.
* **Order** When a function should be executed, ranges from 1 to inf. Please make sure that each number exists once.
* **Plot functionality** Whether your function plots something, or not. When your function allows plotting, two options are available: in the GUI, outside of the GUI, or both. If in the GUI is supported, make sure that the last argument of your function is a figure object. Meaning that your function, when this arguments is given, plots the data in the object given.

* **Dropdown menu** Allows to select an existing function and edit the settings, or select 'Add a new function...' to create a new function
* **+/- button** When new function is being created this button shows a '+'. When pressed a function is added. When an existing function is selected the button shows a '-'. When this button is pressed the function is deleted.
* **Save button** When certain settings have been tweaked, pressing this button saves the changes.
* **Undo button** When settings have been changed, but should not be saved, the Undo button changes these back.

### Plot settings

![Image showing plot settings tab](/images/plotsettings.png)

This tab shows a list of settings for the plots that will be made.
 
* **Figure location** When set to 'Docked', all plots will be created inside of the GUI. When set to 'Undocked' all the figures will be created in a new seperate figure.
* **Class-id** This number is used to describe the clas of the added data. Will increase by 1 when a new matrix of audio recording is added.

### Variable browser

![Image showing variable browser](/images/variablebrowser.png)

This field allows to load audio recordings from the workspace into the GUI. When the system is in 'Single' mode. The variable selected should be a vector containing the data. The vector can be played using the play button to listen to the recording. When using the 'Use variable' function. The current selected variable will be used as audio for all the functions. When 'Current variable' is pressed, the last used variable will be used again, independent of the selected variable in the dropdown.

When the system is in 'Matrix' mode, the variable selected should be a matrix containing multiple audio recordings. Each collumn should contain a recording. When 'Use variable' is pressed, each recording is passed through the functions, and all features extracted will be stored and plotted in the Features and mRMR plot screen. When the 'Add variable' button is pressed, the current selected matrix is added to the previous matrix and used by the functions. Remember, that in between runs the class-id increases automatically, so make sure that the correct class in used to make reading of the Features plot easier.

### Feature plot

![Image showing feature plot](/images/features.png)

A 2D plot of features can be created in this window. All the features can be selected in the two drop down menus on the right.

### Recording audio

![Image showing the audiorecorder](/images/recording.png)

When the button is pressed, an audiorecorder is started. When the light turns red, the recorder is running and the user can speak. The audio recorder uses the settings in the 'Settings' tab.
When the recorder is finished, the variable selector is set to 'Latest recorded in GUI' and all functions are automatically executed. The recorder audio will also be saved in the workspace as 'GUIrecording'.
